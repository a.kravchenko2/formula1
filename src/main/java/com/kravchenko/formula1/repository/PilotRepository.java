package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.Pilot;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PilotRepository extends CrudRepository<Pilot, Integer> {

    @Query("SELECT p FROM Pilot p WHERE p.team = (SELECT t from Team t WHERE t.manager.id = :managerId)")
    List<Pilot> findByQuery(@Param("managerId") int id);
    List<Pilot> findAllByTeamId(int team_id);
}
