package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.EventStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventStatusRepository extends CrudRepository<EventStatus, Integer> {
}
