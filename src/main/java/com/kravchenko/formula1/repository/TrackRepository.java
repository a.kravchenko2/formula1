package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.Track;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrackRepository extends CrudRepository<Track, Integer> {


    List<Track> findAll(Example<Track> of);
}
