package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.Invitation;
import com.kravchenko.formula1.model.Manager;
import com.kravchenko.formula1.model.ManagerTeam;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Integer> {

    @Query("SELECT new com.kravchenko.formula1.model.ManagerTeam(m.id, m.name, t.name) "
            + "FROM Team t INNER JOIN t.manager m")
    List<ManagerTeam> findAllInnerJoin();


    @Query("SELECT new com.kravchenko.formula1.model.ManagerTeam(m.id, m.name, t.name) "
            + "FROM Manager m LEFT JOIN m.team t")
    List<ManagerTeam> findAllLeftJoin();

    List<Manager> findAllById(int id);
}
