package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.Event;
import com.kravchenko.formula1.model.EventStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    Page<Event> findAll(Pageable pageable);
    Page<Event> findAllByStatusEquals(EventStatus status, Pageable pageable);
}
