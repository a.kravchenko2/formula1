package com.kravchenko.formula1.repository;

import com.kravchenko.formula1.model.EventTrack;
import com.kravchenko.formula1.model.Race;
import com.kravchenko.formula1.model.TrackWorkload;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RaceRepository extends CrudRepository<Race, Integer> {

    @Query("SELECT new com.kravchenko.formula1.model.TrackWorkload(COUNT (r.id), t.name) "
            + "FROM Race r  JOIN r.track t GROUP BY t HAVING (COUNT (r.id) > :number)")
    List<TrackWorkload> findWorkloadWithQuery(@Param("number") Long number);

    @Query("SELECT new com.kravchenko.formula1.model.EventTrack(e, t, 0)" +
            "FROM Event e, Track t")
    List<EventTrack> crossJoinWithQuery();

    List<Race> findAllByEventId(int event_id);
}
