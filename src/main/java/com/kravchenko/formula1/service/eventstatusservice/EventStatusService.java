package com.kravchenko.formula1.service.eventstatusservice;

import com.kravchenko.formula1.model.EventStatus;
import com.kravchenko.formula1.repository.EventStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventStatusService implements IEventStatusService {

    @Autowired
    private EventStatusRepository eventStatusRepository;

    @Override
    public List<EventStatus> findAll() {
        return (List<EventStatus>) eventStatusRepository.findAll();
    }

    @Override
    public EventStatus findById(int id) {
        return eventStatusRepository.findById(id).get();
    }
}
