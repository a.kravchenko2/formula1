package com.kravchenko.formula1.service.eventstatusservice;

import com.kravchenko.formula1.model.EventStatus;

import java.util.List;

public interface IEventStatusService {

    List<EventStatus> findAll();
    EventStatus findById(int id);

}
