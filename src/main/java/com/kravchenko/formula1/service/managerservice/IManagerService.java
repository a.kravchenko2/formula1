package com.kravchenko.formula1.service.managerservice;

import com.kravchenko.formula1.model.Invitation;
import com.kravchenko.formula1.model.Manager;
import com.kravchenko.formula1.model.ManagerTeam;

import java.util.List;
import java.util.Set;

public interface IManagerService {

    List<Manager> findAll();
    List<ManagerTeam> findAllLeftJoin();
    List<ManagerTeam> findAllInnerJoin();
    Set<Invitation> findAllInvitationsForId(int id);
}
