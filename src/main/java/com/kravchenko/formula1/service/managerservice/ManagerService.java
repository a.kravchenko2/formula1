package com.kravchenko.formula1.service.managerservice;

import com.kravchenko.formula1.model.Invitation;
import com.kravchenko.formula1.model.Manager;
import com.kravchenko.formula1.model.ManagerTeam;
import com.kravchenko.formula1.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ManagerService implements IManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    @Override
    public List<Manager> findAll() {
        return (List<Manager>) managerRepository.findAll();
    }

    @Override
    public List<ManagerTeam> findAllLeftJoin() {
        return managerRepository.findAllLeftJoin();
    }

    @Override
    public List<ManagerTeam> findAllInnerJoin() {
        return managerRepository.findAllInnerJoin();
    }

    @Override
    public Set<Invitation> findAllInvitationsForId(int id) {
        return managerRepository.findAllById(id).get(0).getInvitations();
    }
}
