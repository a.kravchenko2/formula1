package com.kravchenko.formula1.service.raceservice;

import com.kravchenko.formula1.model.*;

import java.util.List;

public interface IRaceService {

    List<Race> findAll();
    List<FormattedRace> findAllFormatted();
    void deleteById(int id);
    Race save(Race race);
    Race startRaceWithId(int id);

    List<TrackWorkload> findAllTracksWithWorkloadGreaterThan(Long number);
    List<FormattedEventTrack> findAllEventTrackUsage();
}
