package com.kravchenko.formula1.service.raceservice;
import com.kravchenko.formula1.model.*;
import com.kravchenko.formula1.model.weather.NetworkService;
import com.kravchenko.formula1.repository.EventRepository;
import com.kravchenko.formula1.repository.RaceRepository;
import com.kravchenko.formula1.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RaceService implements IRaceService {

    @Autowired
    private RaceRepository raceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TrackRepository trackRepository;

    @Override
    public List<Race> findAll() {
        return ((List<Race>) raceRepository.findAll());
    }

    @Override
    public List<FormattedRace> findAllFormatted() {
        return findAll().stream().map(FormattedRace::new).collect(Collectors.toList());
    }

    @Override
    public Race save(Race race) {

        race.setDate(race.getDate().plusDays(1));

        Optional<Event> event = eventRepository.findById(race.getEvent().getId());
        Optional<Track> track = trackRepository.findById(race.getTrack().getId());

        if (!event.isPresent() || !track.isPresent()) {
            return null;
        }

        race.setEvent(event.get());
        race.setTrack(track.get());

        NetworkService.fetchWeatherAsync(race.getTrack().getPoint(), weather -> {
            if (null == weather) {
                raceRepository.save(race);
                return;
            }
            race.setWeatherConditions(weather.toString());
            raceRepository.save(race);
        });

        return new Race();
    }

    public Race startRaceWithId(int id) {
       return null;
    }

    @Override
    public List<TrackWorkload> findAllTracksWithWorkloadGreaterThan(Long number) {
        return raceRepository.findWorkloadWithQuery(number);
    }

    @Override
    public void deleteById(int id) {
        raceRepository.deleteById(id);
    }

    @Override
    public List<FormattedEventTrack> findAllEventTrackUsage() {
        List<EventTrack> eventTrackArray = raceRepository.crossJoinWithQuery();
        for (EventTrack eventTrack :
                eventTrackArray) {
            int eventId = eventTrack.getEvent().getId();
            for (Race race :
                    raceRepository.findAllByEventId(eventId)) {
                eventTrack.setCount(eventTrack.getCount() + (eventTrack.getTrack().getId() == race.getTrack().getId() ? 1 : 0));
            }
        }
       return eventTrackArray.stream().map(FormattedEventTrack::new).collect(Collectors.toList());
    }
}
