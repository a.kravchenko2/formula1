package com.kravchenko.formula1.service.trackservice;
import com.kravchenko.formula1.model.Track;

import java.util.List;

public interface ITrackService {

    List<Track> findAll(String name, String type);
    Track findById(int id);
}
