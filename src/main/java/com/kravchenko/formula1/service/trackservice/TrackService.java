package com.kravchenko.formula1.service.trackservice;

import com.kravchenko.formula1.model.Track;
import com.kravchenko.formula1.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
public class TrackService implements  ITrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Override
    public List<Track> findAll(String name, String type) {
        ExampleMatcher exampleMatcher = ExampleMatcher
                .matchingAll()
                .withMatcher("name", contains().ignoreCase())
                .withMatcher("type", contains().ignoreCase())
                .withMatcher("point", contains().ignoreCase())
                .withMatcher("races", contains().ignoreCase());


        Track trackToFind = new Track(name, type);
        return trackRepository.findAll(Example.of(trackToFind, exampleMatcher)); }

    @Override
    public Track findById(int id) {
        return trackRepository.findById(id).get();
    }
}
