package com.kravchenko.formula1.service.eventservice;

import com.kravchenko.formula1.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IEventService {

    List<Event> findAll();

    Page<Event> findAll(int pageNumber,
                        int pageSize);
    Page<Event> findAll(int pageNumber,
                        int pageSize,
                        String sortBy);
    Event save(Event event);
    void deleteById(int id);
    Page<Event> findAllNowSorted(int pageNumber,
                                 int pageSize,
                                 String sortBy);
}
