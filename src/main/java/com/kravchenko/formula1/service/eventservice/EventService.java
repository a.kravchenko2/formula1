package com.kravchenko.formula1.service.eventservice;

import com.kravchenko.formula1.model.Event;
import com.kravchenko.formula1.model.EventStatus;
import com.kravchenko.formula1.model.EventStatusType;
import com.kravchenko.formula1.repository.EventRepository;
import com.kravchenko.formula1.repository.EventStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class EventService implements  IEventService {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    private EventStatusRepository eventStatusRepository;

    @Override
    public Page<Event> findAll(int pageNumber,
                               int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("id").descending());
        return eventRepository.findAll(pageable);
    }

    @Override
    public Page<Event> findAll(int pageNumber,
                               int pageSize,
                               String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy).descending());
        return eventRepository.findAll(pageable);
    }

    @Override
    public Event save(Event event) {

        event.setStartDate(event.getStartDate().plusDays(1));
        event.setEndDate(event.getEndDate().plusDays(1));

        if (event.getEventName().equals("")) {
            return null;
        }

        if (event.getEndDate().isBefore(event.getStartDate())) {
            return null;
        }

        if (event.getStartDate().isAfter(LocalDate.now())) {
            Optional<EventStatus> status = eventStatusRepository.findById(EventStatusType.COMING.rawValue);
            event.setStatus(status.get());
            return eventRepository.save(event);
        }

        if (event.getEndDate().isBefore(LocalDate.now())) {
            Optional<EventStatus> status = eventStatusRepository.findById(EventStatusType.FINISHED.rawValue);
            event.setStatus(status.get());
            return eventRepository.save(event);
        }

        if (event.getEndDate().isAfter(LocalDate.now()) && event.getStartDate().isBefore(LocalDate.now())) {
            Optional<EventStatus> status = eventStatusRepository.findById(EventStatusType.NOW.rawValue);
            event.setStatus(status.get());
            return eventRepository.save(event);
        }

        return null;
    }

    @Override
    public List<Event> findAll() {
        return (List<Event>) eventRepository.findAll();
    }

    @Override
    public void deleteById(int id) {
        eventRepository.deleteById(id);
    }

    @Override
    public Page<Event> findAllNowSorted(int pageNumber, int pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy).descending());
        var status = eventStatusRepository.findById(EventStatusType.NOW.rawValue).get();
        return eventRepository.findAllByStatusEquals(status, pageable);
    }

}
