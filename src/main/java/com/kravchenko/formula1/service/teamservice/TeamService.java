package com.kravchenko.formula1.service.teamservice;

import com.kravchenko.formula1.model.Team;
import com.kravchenko.formula1.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService implements ITeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<Team> findAll() {
        return  (List<Team>) teamRepository.findAll();
    }
}
