package com.kravchenko.formula1.service.teamservice;

import com.kravchenko.formula1.model.Team;

import java.util.List;

public interface ITeamService {

    List<Team> findAll();
}
