package com.kravchenko.formula1.service.resultservice;

import com.kravchenko.formula1.model.Result;

import java.util.List;

public interface IResultService {

    List<Result> findAll();
    Result save(Result result);

}
