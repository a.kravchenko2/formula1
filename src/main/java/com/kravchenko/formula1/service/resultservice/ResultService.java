package com.kravchenko.formula1.service.resultservice;

import com.kravchenko.formula1.model.Result;
import com.kravchenko.formula1.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultService  implements IResultService {

    @Autowired
    ResultRepository resultRepository;

    @Override
    public List<Result> findAll() {
        return (List<Result>) resultRepository.findAll();
    }

    @Override
    public Result save(Result result) {
        return resultRepository.save(result);
    }
}
