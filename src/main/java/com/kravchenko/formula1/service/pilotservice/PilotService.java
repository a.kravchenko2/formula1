package com.kravchenko.formula1.service.pilotservice;

import com.kravchenko.formula1.model.Pilot;
import com.kravchenko.formula1.repository.PilotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PilotService implements IPilotService {

    @Autowired
    private PilotRepository pilotRepository;

   @Override
    public List<Pilot> findAll() {
       return (List<Pilot>) pilotRepository.findAll();
   }

    @Override
    public List<Pilot> findWithTeamId(int id) {
        return pilotRepository.findAllByTeamId(id);
    }

    @Override
    public List<Pilot> findWithManagerId(int id) {
        return pilotRepository.findByQuery(id);
    }
}
