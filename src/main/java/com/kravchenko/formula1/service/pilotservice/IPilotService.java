package com.kravchenko.formula1.service.pilotservice;

import com.kravchenko.formula1.model.Pilot;

import java.util.List;

public interface IPilotService {

    List<Pilot> findAll();
    List<Pilot> findWithTeamId(int id);
    List<Pilot> findWithManagerId(int id);

}
