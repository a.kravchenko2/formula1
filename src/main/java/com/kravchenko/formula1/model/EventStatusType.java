package com.kravchenko.formula1.model;

public enum EventStatusType {

    FINISHED(1),
    COMING(2),
    NOW(3);

    public final int rawValue;

    EventStatusType(final int rawValue) {
        this.rawValue = rawValue;
    }

}
