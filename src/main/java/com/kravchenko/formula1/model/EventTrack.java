package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventTrack {

    private Event event;

    private Track track;

    private int count;

    public EventTrack(Event event, Track track, int count) {
        this.event = event;
        this.track = track;
        this.count = count;
    }
}
