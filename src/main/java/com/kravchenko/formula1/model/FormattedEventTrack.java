package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FormattedEventTrack {

    private String eventName;

    private String trackName;

    private int count;

    public FormattedEventTrack(EventTrack eventTrack) {
        this.eventName = eventTrack.getEvent().getEventName();
        this.trackName = eventTrack.getTrack().getName();
        this.count = eventTrack.getCount();
    }
}
