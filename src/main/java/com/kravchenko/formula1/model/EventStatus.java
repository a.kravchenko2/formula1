package com.kravchenko.formula1.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "event_status")
@NoArgsConstructor
public class EventStatus {

    @Id
    @Getter @Setter
    private int id;

    @Column(name = "status")
    @Getter @Setter
    private String status;

    @OneToMany(mappedBy = "status")
    private Set<Event> events;
}
