package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "results")
@NoArgsConstructor
public class Result {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JoinColumn(name = "race_id")
    @Getter @Setter
    private int raceId;

    @JoinColumn(name = "pilot_id")
    @Getter @Setter
    private int pilotId;

    @JoinColumn(name = "status")
    @Getter @Setter
    private int status;

    @Column(name = "time")
    @Getter @Setter
    private LocalTime time;

    @Column(name = "place")
    @Getter @Setter
    private Integer place;

}
