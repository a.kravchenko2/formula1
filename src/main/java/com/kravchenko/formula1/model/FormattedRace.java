package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
public class FormattedRace {

    private int id;

    private String event;

    private String type;

    private LocalDate date;

    private String weatherConditions;

    private String track;

    private Boolean canStart = false;

    public FormattedRace(Race race) {
        this.id = race.getId();
        this.event = race.getEvent().getEventName();
        this.type = race.getType();
        this.date = race.getDate();
        this.weatherConditions = race.getWeatherConditions();
        this.track = race.getTrack().getName();
        this.canStart = race.getEvent().getStatus().getId() == EventStatusType.NOW.rawValue;
    }
}
