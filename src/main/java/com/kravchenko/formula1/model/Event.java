package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "events")
@NoArgsConstructor
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private int id;

    @ManyToOne
    @JoinColumn(name = "status")
    @Getter @Setter
    @NotNull
    private EventStatus status;

    @Column(name = "start_date")
    @Getter @Setter
    @NotNull
    private LocalDate startDate;

    @Column(name = "end_date")
    @Getter @Setter
    @NotNull
    private LocalDate endDate;

    @Column(name = "event_name")
    @Getter @Setter
    @NotNull
    private String eventName;

    @OneToMany(mappedBy = "event")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Race> races;

    @OneToOne(mappedBy = "event")
    private Invitation invitation;

}
