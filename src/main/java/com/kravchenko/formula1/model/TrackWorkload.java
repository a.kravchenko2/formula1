package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TrackWorkload {

    private Long racesCount;
    private String trackName;

    public TrackWorkload(Long racesCount, String trackName) {
        this.racesCount = racesCount;
        this.trackName = trackName;
    }
}
