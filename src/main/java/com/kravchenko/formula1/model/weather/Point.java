package com.kravchenko.formula1.model.weather;

import lombok.*;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Point implements Serializable {

    private Float lat;
    private Float lng;
}
