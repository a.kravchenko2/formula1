package com.kravchenko.formula1.model.weather;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class JsonDecoder {

    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.disable(FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @SneakyThrows
    public static <T> T decode(String jsonString, Class<T> classType) {
        return mapper.readValue(jsonString, classType);
    }

    @SneakyThrows
    public static <T> T decode(String jsonString, TypeReference<T> classTypeRef) {
        return mapper.readValue(jsonString, classTypeRef);
    }

}
