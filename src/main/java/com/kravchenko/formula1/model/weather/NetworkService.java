package com.kravchenko.formula1.model.weather;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;


public class NetworkService {
    private static final Properties properties = new Properties();
    private static final OkHttpClient httpClient = new OkHttpClient();
    private static final int SUCCESS = 200;

    private static Request buildWeatherRequest(Point point) {
        HttpUrl url = HttpUrl.parse(properties.getProperty("OPENWEATHERMAP_API")).newBuilder()
                .addQueryParameter("lat", String.valueOf(point.getLat()))
                .addQueryParameter("lon", String.valueOf(point.getLng()))
                .addQueryParameter("units", "metric")
                .addQueryParameter("lang", "en")
                .addQueryParameter("appid", properties.getProperty("OPENWEATHERMAP_API_KEY"))
                .build();
        return new Request.Builder().url(url).get().build();
    }

    @SneakyThrows
    public static void fetchWeatherAsync(Point point, Consumer<Weather> onCompletion) {
        InputStream stream = ClassLoader.getSystemResourceAsStream("api.properties");
        if (null != stream) {
            properties.load(stream);
        }
        CompletableFuture.supplyAsync(() -> {
            Response response;
            try {
                Request request = buildWeatherRequest(point);
                response = httpClient.newCall(request).execute();
            } catch (Exception e) {
                onCompletion.accept(null);
                return null;
            }
            if (SUCCESS != response.code()) {
                onCompletion.accept(null);
                return null;
            }
            try {
                Weather weather = JsonDecoder.decode(Objects.requireNonNull(response.body()).string(), Weather.class);
                onCompletion.accept(weather);
            } catch (IOException e) {
                onCompletion.accept(null);
                return null;
            }
            return null;
        });
    }
}
