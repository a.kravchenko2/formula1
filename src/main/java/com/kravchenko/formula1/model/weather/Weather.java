package com.kravchenko.formula1.model.weather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Weather {
    @JsonProperty("weather")
    private List<GeneralWeather> general;
    @JsonProperty("main")
    private WeatherParameters parameters;
    private int visibility;
    private Wind wind;
    private Clouds clouds;

    @Getter
    @Setter
    public static final class GeneralWeather {
        private String main;
        private String description;
    }

    @Getter
    @Setter
    public final class WeatherParameters {
        @JsonProperty("temp")
        private double temperature;
        @JsonProperty("feels_like")
        private double feelsLikeTemperature;
        @JsonProperty("temp_min")
        private double minTemperature;
        @JsonProperty("temp_max")
        private double maxTemperature;
        private int pressure;
        private int humidity;
        @JsonProperty("sea_level")
        private int seaLevel = 0;
        @JsonProperty("grnd_level")
        private int groundLevel = 0;
    }

    @Getter
    @Setter
    public final class Wind {
        private double speed;
        private @JsonProperty("deg")
        int degree;
        private double gust;
    }

    @Getter
    @Setter
    public final class Clouds {
        private @JsonProperty("all")
        int clouds;
    }

    @Override
    public String toString() {
        return "T: " + parameters.temperature + " C" + ", " +
               "H: " + parameters.humidity + " %";
    }
}

