package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Table(name = "managers")
public class Manager {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotNull
    @Getter @Setter
    private String name;

    @OneToOne(mappedBy = "manager")
    private Team team;

    @ManyToMany
    @JoinTable(name = "messages",
            joinColumns = @JoinColumn(name = "manager_id"),
            inverseJoinColumns = @JoinColumn(name = "invitation_id")
    )
    @Setter @Getter
    private Set<Invitation> invitations = new HashSet<>();

//    public void addInvitation(Invitation invitation){
//        this.invitations.add(invitation);
//        invitation.getManagers().add(this);
//    }
//
//    public void removeInvitation(Invitation invitation){
//        this.invitations.remove(invitation);
//        invitation.getManagers().remove(this);
//    }
}

