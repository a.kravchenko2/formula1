package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "pilots")
public class Pilot {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "full_name")
    @NotNull
    @Getter @Setter
    private String fullName;

    @ManyToOne
    @Getter
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    private Team team;

}

