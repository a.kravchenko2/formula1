package com.kravchenko.formula1.model;

import com.kravchenko.formula1.model.weather.Point;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tracks")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@NoArgsConstructor
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Integer id;

    @Column(name = "name")
    @Getter @Setter
    private String name;

    @Column(name = "type")
    @Getter @Setter
    private String type;

//    @Column(name = "lat")
//    @Getter @Setter
//    private Float lat;
//
//    @Column(name = "lng")
//    @Getter @Setter
//    private Float lng;

    @Type(type = "jsonb")
    @Column(name = "coordinates", columnDefinition = "jsonb")
    @Getter @Setter
    private Point point;

    @OneToMany(mappedBy = "track")
    private Set<Race> races;

    public Track(String name, String type) {
        this.name = name;
        this.type = type;
    }
}
