package com.kravchenko.formula1.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "races")
@NoArgsConstructor
@Getter @Setter
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    @NotNull
    private Event event;

    @Column(name = "type")
    @NotNull
    private String type;

    @Column(name = "date")
    @NotNull
    private LocalDate date;

    @Column(name = "weather_conditions")
    @NotNull
    private String weatherConditions;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "track_id", referencedColumnName = "id")
    private Track track;

}
