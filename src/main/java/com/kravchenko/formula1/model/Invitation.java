package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "invitations")
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "date")
    @NotNull
    @Getter
    @Setter
    private LocalDate date;

    @Column(name = "time")
    @NotNull
    @Getter @Setter
    private LocalTime time;

    @OneToOne
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    private Event event;

    @ManyToMany(mappedBy = "invitations")
    private Set<Manager> managers = new HashSet<>();

//    public void addManager(Manager manager){
//        this.managers.add(manager);
//        manager.getInvitations().add(this);
//    }
//
//    public void removeManager(Manager manager){
//        this.managers.remove(manager);
//        manager.getInvitations().remove(this);
//    }
}
