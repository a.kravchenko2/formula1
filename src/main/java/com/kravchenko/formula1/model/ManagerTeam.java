package com.kravchenko.formula1.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ManagerTeam {

    private int id;
    private String managerName;
    private String teamName;

    public ManagerTeam(int id, String managerName, String teamName) {
        this.id = id;
        this.managerName = managerName;
        this.teamName = teamName;
    }

}
