package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Pilot;
import com.kravchenko.formula1.service.pilotservice.IPilotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/pilot")
public class PilotController {

    @Autowired
    private IPilotService pilotService;

    @GetMapping("/showAll")
    public List<Pilot> findPilots() {
        return pilotService.findAll();
    }

    @GetMapping("/team")
    @ResponseBody
    public List<Pilot> findPilotsWithTeamId(@RequestParam String id) {
        return pilotService.findWithTeamId(Integer.parseInt(id));
    }

    @GetMapping("/manager")
    @ResponseBody
    public List<Pilot> findPilotsWithManagerId(@RequestParam String id) {
        return pilotService.findWithManagerId(Integer.parseInt(id));
    }
}
