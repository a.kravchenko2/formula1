package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.*;
import com.kravchenko.formula1.repository.EventRepository;
import com.kravchenko.formula1.repository.TrackRepository;
import com.kravchenko.formula1.service.raceservice.IRaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/race")
public class RaceController {

    @Autowired
    private IRaceService raceService;

    @GetMapping("/showAll")
    public List<FormattedRace> findRaces() {
        return raceService.findAllFormatted();
    }

    @PostMapping("/save")
    public int post(@RequestBody  Race race) {
        if (raceService.save(race) == null) {
            return 500;
        }
        return 200;
    }

    @PostMapping("/saveMany")
    public int post(@RequestBody  List<Race> races) {
        int result = 500;
        for (Race race :
                races) {
            if (raceService.save(race) == null) {
                result = 500;
            } else {
                result = 200;
            }
        }
        return result;
    }

    @PostMapping("/delete")
    public int delete(@RequestParam String id) {
        raceService.deleteById(Integer.parseInt(id));
        return 200;
    }

    @GetMapping("/track-workload")
    public  List<TrackWorkload> findTrackWorkload() {
        return raceService.findAllTracksWithWorkloadGreaterThan(0L);
    }

    @GetMapping("/event-track")
    public List<FormattedEventTrack> findEventTrackUsage() {
        return raceService.findAllEventTrackUsage();
    }
}
