package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Event;
import com.kravchenko.formula1.service.eventservice.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/event")
public class EventController {

    private final int PAGE_SIZE = 3;

    @Autowired
    private IEventService eventService;

    @PostMapping("/save")
    public int post(@RequestBody Event event) {

        if (eventService.save(event) == null) {
            return 500;
        }
        return 200;
    }

    @PostMapping("/edit")
    public int edit(@RequestBody Event event) {

        if (eventService.save(event) == null) {
            return 500;
        }
        return 200;
    }

    @PostMapping("/delete")
    public int delete(@RequestParam String id) {
        eventService.deleteById(Integer.parseInt(id));
        return 200;
    }

    @GetMapping("/showAll")
    public List<Event> findAll() {
        return eventService.findAll();
    }

    @GetMapping("/page/{pageNumber}/{sortBy}")
    public Page<Event> findPaginatedSorted(@PathVariable(value = "pageNumber") int pageNumber,
                                           @PathVariable(value = "sortBy") String sortBy) {
        return eventService.findAll(pageNumber, PAGE_SIZE, sortBy);
    }

    @GetMapping("page/now-sorted/{pageNumber}/{sortBy}")
    public Page<Event> findPaginatedNowSorted(@PathVariable(value = "pageNumber") int pageNumber,
                                              @PathVariable(value = "sortBy") String sortBy) {
        return eventService.findAllNowSorted(pageNumber, PAGE_SIZE, sortBy);
    }

}
