package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Team;
import com.kravchenko.formula1.service.teamservice.ITeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private ITeamService teamService;

    @GetMapping("/showAll")
    public List<Team> findTeams() {
        return  teamService.findAll();
    }
}
