package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Track;
import com.kravchenko.formula1.service.trackservice.ITrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/track")
public class TrackController {

    @Autowired
    private ITrackService trackService;

    @GetMapping("/showAll")
    public List<Track> findTracks(@RequestParam(name = "name") String name,
                                  @RequestParam(name="type") String type) {
        return trackService.findAll(name, type);
    }

}
