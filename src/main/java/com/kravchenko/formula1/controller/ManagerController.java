package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Invitation;
import com.kravchenko.formula1.model.Manager;
import com.kravchenko.formula1.model.ManagerTeam;
import com.kravchenko.formula1.service.managerservice.IManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private IManagerService managerService;

    @GetMapping("/showAll")
    public List<Manager> findAll() {
        return managerService.findAll();
    }

    @GetMapping("/showAll-inner")
    public List<ManagerTeam> findAllInnerJoin() {
      return managerService.findAllInnerJoin();
    }

    @GetMapping("/showAll-left")
    public List<ManagerTeam> findAllLeftJoin() {
        return managerService.findAllLeftJoin();
    }

    @GetMapping("/invitations")
    public Set<Invitation> findAllInvitations(@RequestParam String id) {
        return managerService.findAllInvitationsForId(Integer.parseInt(id));
    }
}
