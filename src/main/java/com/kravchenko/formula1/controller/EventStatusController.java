package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.EventStatus;
import com.kravchenko.formula1.service.eventstatusservice.IEventStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/event_status")
public class EventStatusController {

    @Autowired
    IEventStatusService eventStatusService;

    @GetMapping("/showAll")
    public List<EventStatus> findAll() {
        return  eventStatusService.findAll();
    }

}
