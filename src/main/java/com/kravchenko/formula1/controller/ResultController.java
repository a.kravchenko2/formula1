package com.kravchenko.formula1.controller;

import com.kravchenko.formula1.model.Race;
import com.kravchenko.formula1.model.Result;
import com.kravchenko.formula1.service.resultservice.IResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/result")
public class ResultController {

    @Autowired
    IResultService resultService;

    @GetMapping("/showAll")
    public List<Result> findResults() {
        return resultService.findAll();
    }

    @PostMapping("/save")
    public void post(@RequestBody Result result) {
        resultService.save(result);
    }

}
