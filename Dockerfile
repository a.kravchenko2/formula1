FROM openjdk:18
ADD target/formula1-app.jar formula1-app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","formula1-app.jar"]